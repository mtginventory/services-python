import http.client
from aws_xray_sdk.core import xray_recorder
import boto3
import math, time
from aws_xray_sdk.core import patch_all
import logging

patch_all()

client = http.client.HTTPSConnection("dev-oj0ok9xc.us.auth0.com")
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table("mtg-inventory-auth-cache")


@xray_recorder.capture('get-data-from-auth0')
def get_user_info(token, retry_count=0):
    try:
        client.request('GET', '/userinfo', headers={'Authorization': 'Bearer {}'.format(token)})
        response = client.getresponse()
        body = response.read().decode()
    except http.client.RemoteDisconnected:
        if retry_count < 5:
            logging.warning("retrying for %s time", retry_count + 1)
            return get_user_info(token, retry_count + 1)
        return {"statusCode": 500, "body": "Error connecting to 0auth"}
    if response.status < 300:
        table.put_item(Item={
            'token': token,
            'auth0Authentication': body,
            'ttl': math.trunc(time.time()) + 5 * 60
        })
    return {"statusCode": response.status, "body": body}


@xray_recorder.capture('get-cached-user')
def get_cached_user(token):
    cache = table.get_item(Key={'token': token}).get("Item")
    if cache and 'ttl' in cache:
        if cache['ttl'] > time.time():
            return cache['auth0Authentication']


@xray_recorder.capture('authentication-lambda')
def handle_lambda(event, context):
    token = event.get('token')
    if not token:
        return {"statusCode": 401, "body": "You need to provide a token for authentication"}
    if token[:7] == 'Bearer ':
        token = token[7:]
    user = get_cached_user(token)
    if user:
        return {"statusCode": 200, "body": user}
    return get_user_info(token)
