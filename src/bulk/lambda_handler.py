import shutil
import tempfile
from datetime import datetime
import http.client
import urllib
import os
import logging
import json

import botocore
import boto3
from aws_xray_sdk.core import patch_all
from dateutil.tz import tzlocal

logger = logging.getLogger('main')
logger.setLevel('DEBUG')
client = http.client.HTTPSConnection('api.scryfall.com')
s3 = boto3.resource('s3')
data_directory_root = 'card-data'
bucket = 'mtginventory-content'
datetime_format = '%Y-%m-%dT%H:%M:%S.%f%z'

sqs = boto3.resource('sqs')
card_price_queue_name = os.getenv("CARD_PRICE_QUEUE")
card_price_queue = sqs.get_queue_by_name(QueueName=card_price_queue_name)


def camel_case_names(obj):
    for key in list(obj):
        if isinstance(obj[key], dict):
            camel_case_names(obj[key])
        if '_' in key:
            new_key = "".join([k.capitalize() for k in key.split('_')])
            new_key = new_key[0].lower() + new_key[1:]
            obj[new_key] = obj.pop(key)


def format_number_fields(obj):
    for field in ['usd', 'usdFoil', 'tix', 'eur']:
        if field not in obj or obj[field] is None:
            obj[field] = 0


def get_latest_cards_file():
    client.request('GET', '/bulk-data/default_cards')
    response = client.getresponse().read().decode()
    return json.loads(response)


def get_latest_cards_data(latest_cards_file):
    uri = latest_cards_file['download_uri']
    response = urllib.request.urlopen(uri)
    for line_bytes in response:
        line = line_bytes.decode().strip()
        if line.startswith('{'):
            try:
                if line.endswith(','):
                    card_data = json.loads(line[:-1])
                else:
                    card_data = json.loads(line)
                camel_case_names(card_data)
                format_number_fields(card_data['prices'])
                yield card_data
            except Exception as e:
                logger.error(line)
                logger.error(e)


def write_to_set_file(obj):
    path = f'{data_directory_root}/sets/{obj["set"]}'
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'a', encoding='utf-8') as set_file:
        json.dump(obj, set_file)
        set_file.write("\n")


def write_to_id_file(obj):
    path = f'{data_directory_root}/ids/{obj["id"][0:2]}'
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'a', encoding='utf-8') as set_file:
        json.dump(obj, set_file)
        set_file.write("\n")


def get_price_file_key(latest_cards_file):
    updated_at = datetime.strptime(latest_cards_file['updated_at'], datetime_format)
    timestamp_string = datetime.strftime(updated_at, '%Y%m%d%H%M%S')
    return f'{data_directory_root}/prices/{timestamp_string}.csv'


def price_file_exists(latest_cards_file):
    key = get_price_file_key(latest_cards_file)
    logger.debug(f'using key: {key}')
    obj = s3.Object(bucket, key)
    try:
        obj.get()
        return True
    except botocore.exceptions.ClientError as e:
        return False


def should_update_cards(latest_cards_file):
    updated_at = datetime.strptime(latest_cards_file['updated_at'], datetime_format)
    key = f'{data_directory_root}/last_card_update_time'
    obj = s3.Object(bucket, key)
    try:
        data = obj.get()['Body'].read()
        last_update_time = datetime.strptime(data.decode(), datetime_format)
        return (updated_at - last_update_time).days > 7
    except botocore.exceptions.ClientError as e:
        return True


def set_update_time():
    key = f'{data_directory_root}/last_card_update_time'
    obj = s3.Object(bucket, key)
    timestamp = datetime.now(tz=tzlocal()).strftime(datetime_format)
    obj.put(Body=timestamp.encode())


def update_card_data():
    latest_cards_file = get_latest_cards_file()
    update_prices = not price_file_exists(latest_cards_file)
    update_cards = should_update_cards(latest_cards_file)
    if update_prices:
        logger.info("Updating prices")
        if update_cards:
            logger.info("Updating cards")
        path = get_price_file_key(latest_cards_file)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as price_data_file:
            price_data_file.write('id,update_time,usd,eur,tix,usdFoil\n')
            entries = []
            for data in get_latest_cards_data(latest_cards_file):
                if update_cards:
                    write_to_id_file(data)
                    write_to_set_file(data)
                prices = data['prices']
                data_line = f"{data['id']},{latest_cards_file['updated_at']},{prices['usd']},{prices['eur']},{prices['tix']},{prices['usdFoil']}"
                price_data_file.write(data_line + "\n")
                entries.append({'Id': data['id'] , 'MessageBody': data_line})
                if len(entries) == 10:
                    card_price_queue.send_messages(Entries=entries)
                    entries = []
        if len(entries) > 0:
            card_price_queue.send_messages(Entries=entries)
        for directory, _directories, files in os.walk(data_directory_root):
            for file in files:
                object_path = f'{directory}/{file}'
                obj = s3.Object(bucket, object_path)
                with open(object_path, 'r', encoding='utf-8') as f:
                    obj.put(Body=f.read())
        set_update_time()
        shutil.rmtree(data_directory_root)
    else:
        logger.info("Prices were already up to date, not updating prices or cards")


def handle_update_cards(event, context):
    with tempfile.TemporaryDirectory() as tempdir:
        os.chdir(tempdir)
        update_card_data()


patch_all()
