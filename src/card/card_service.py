import json
import http.client
import re
import urllib
import logging
from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()
logger = logging.getLogger('card.lambda_handler')
logger.setLevel('DEBUG')


@xray_recorder.capture('get-autocomplete-names')
def get_autocomplete_names(query):
    client = http.client.HTTPSConnection("api.scryfall.com")
    client.request('GET', '/cards/autocomplete?{}'.format(urllib.parse.urlencode(({'q': query}))))
    response = client.getresponse().read().decode()
    return json.loads(response)['data']


def camel_case_names(obj):
    for key in list(obj):
        if isinstance(obj[key], dict):
            camel_case_names(obj[key])
        if '_' in key:
            new_key = "".join([k.capitalize() for k in key.split('_')])
            new_key = new_key[0].lower() + new_key[1:]
            obj[new_key] = obj.pop(key)


def expand_values(value):
    if '-' in value:
        start, end = [int(v.strip()) for v in value.split('-')]
        return list(range(start, end + 1))
    return [value]


@xray_recorder.capture('get-search-query')
def get_search_query(query):
    if re.match("^[A-Z0-9a-z]{3}:[0-9-,\s]+$", query):
        set_id = query[:3]
        values = [expand_values(v.strip()) for v in query[4:].split(',')]
        values = ["cn:{}".format(i) for sublist in values for i in sublist]
        collector_numbers = " or ".join(values)
        return 'e:{} ({})'.format(set_id, collector_numbers)
    elif re.match('[A-Z0-9a-z]{3}:.*', query):
        set_id = query[:3]
        name_query = query[4:]
        if len(name_query) < 2:
            return None
        autocomplete_names = get_autocomplete_names(name_query)
        if len(autocomplete_names) == 0:
            return None
        return f'e:{set_id} (' + " or ".join(['!"{}"'.format(n) for n in autocomplete_names]) + ')'

    autocomplete_names = get_autocomplete_names(query)
    if len(autocomplete_names) == 0:
        return None
    return " or ".join(['!"{}"'.format(n) for n in autocomplete_names])


@xray_recorder.capture('get-cards')
def get_autocomplete_cards(query):
    search_query = get_search_query(query)
    logger.info("Querying scryfall with %s", search_query)
    if search_query is None:
        return []
    client = http.client.HTTPSConnection("api.scryfall.com")
    client.request('GET', "/cards/search?{}".format(urllib.parse.urlencode({'q': search_query, 'unique': 'prints'})))
    response = client.getresponse().read().decode()
    data = json.loads(response).get('data')
    if data is None:
        return []
    for o in data:
        camel_case_names(o)
    return data


def handle_autocomplete(event, context):
    if 'q' in event['queryStringParameters']:
        return {'statusCode': 200, 'body': json.dumps(get_autocomplete_cards(event['queryStringParameters']['q']))}
    return {'statusCode': 400, 'body': '{"error": "You must provide a query"}'}


cache = {}


def handle_get_card(event, context):
    format_response = False
    if 'pathParameters' in event:
        logger.info("Event came from API so formatting response and using path parameters")
        format_response = True
        event = event['pathParameters']
    if 'id' in event:
        url = '/cards/{}'.format(event['id'])
    elif 'set' in event and 'number' in event:
        url = '/cards/{}/{}'.format(event['set'], event['number']).lower()
    else:
        raise ValueError("Either [id] or [set,number] is required as an input")
    if url in cache:
        data = cache.get(url)
    else:
        client = http.client.HTTPSConnection("api.scryfall.com")
        client.request('GET', url)
        response = client.getresponse().read().decode()
        data = json.loads(response)
        camel_case_names(data)
        cache[url] = data
    if format_response:
        return {'statusCode': 200, 'body': json.dumps(data)}
    return data


def handle_get_set_cards(event, context):
    set_id = event['pathParameters']['set']
    has_more = True
    page = 1
    cards = []
    client = http.client.HTTPSConnection("api.scryfall.com")
    while has_more:
        client.request('GET', "/cards/search?{}".format(urllib.parse.urlencode({'q': f'e:{set_id}', 'page': page,
                                                                                'order': 'set', 'unique': 'prints'})))
        response = client.getresponse().read().decode()
        response = json.loads(response)
        cards += response['data']
        has_more = response['has_more']
        page += 1
    for c in cards:
        camel_case_names(c)
    return {'statusCode': 200, 'body': json.dumps(cards)}
