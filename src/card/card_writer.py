import json
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute
from dateutil.parser import parse


class CardPrice(Model):
    def __init__(self, *initial_data, **kwargs):
        """The model class that contains the card details in an inventory for dynamodb."""
        super().__init__(**kwargs)
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    class Meta:

        """The meta details for the dynamodb model."""
        table_name = 'mtg-card-price'

    cardId = UnicodeAttribute(hash_key=True)
    timestamp = NumberAttribute(range_key=True)
    usd = NumberAttribute(default_for_new=0)
    eur = NumberAttribute(default_for_new=0)
    tix = NumberAttribute(default_for_new=0)
    usdFoil = NumberAttribute(default_for_new=0)


def handle_lambda(event, context):
    for record in event['Records']:
        if record['body'].startswith('{'):
            body = json.loads(record['body'])
            card_price = CardPrice(body)
        else:
            [card_id, updated_at, usd, eur, tix, usd_foil] = record['body'].split(',')
            timestamp = parse(updated_at).timestamp()*1000
            card_price = CardPrice(cardId=card_id, timestamp=timestamp, usd=float(usd), eur=float(eur), tix=float(tix), usdFoil=float(usd_foil))
        card_price.save()
