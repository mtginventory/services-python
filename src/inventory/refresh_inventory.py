from aws_xray_sdk.core import patch_all
import os.path
import json
import boto3
import time
from dateutil.parser import parse

from models import InventoryCard, CardPrice, InventoryValue

import logging

timestamp_format = '%Y%m%d%H%M%S'
s3 = boto3.resource('s3')
data_directory_root = 'card-data/prices'
bucket = 'mtginventory-content'
logging.getLogger().setLevel("DEBUG")
sqs = boto3.resource('sqs')
cache_break = '2'
refresh_queue_name = os.getenv("REFRESH_INVENTORY_VALUE_QUEUE")
if refresh_queue_name is not None:
    refresh_queue = sqs.get_queue_by_name(QueueName=refresh_queue_name)


def format_price_cache_line(card_price):
    return '{0}|{1}'.format(card_price.timestamp, json.dumps(card_price.to_dict()))


def parse_price_cache_line(line):
    return CardPrice(json.loads(line[line.index('|') + 1:]))


def get_prices(card_id):
    cache_file_path = '/tmp/{0}card-prices-{1}'.format(cache_break, card_id)
    if os.path.exists(cache_file_path):
        with open(cache_file_path) as f:
            card_prices = []
            for line in f.readlines():
                card_prices.append(parse_price_cache_line(line))
            if len(card_prices) > 0:
                return card_prices
    card_prices = list(CardPrice.query(card_id))
    lines = [format_price_cache_line(c) + '\n' for c in card_prices]
    with open(cache_file_path, 'w') as f:
        f.writelines(lines)
    return card_prices


def get_inventory_cards(inventory_id):
    cache_file_path = '/tmp/{0}inventory-{1}'.format(cache_break, inventory_id)
    logging.debug("using cache file %s", cache_file_path)
    if os.path.exists(cache_file_path):
        logging.debug("cache exists, using it")
        with open(cache_file_path) as f:
            content = f.read()
            if len(content) > 0:
                return [InventoryCard(d) for d in json.loads(content)]
            else:
                logging.debug("cache was empty")
    else:
        logging.debug("cache did not exist")
    cards = list(InventoryCard.query(inventory_id,
                                     filter_condition=(InventoryCard.quantity > 0) | (InventoryCard.foilQuantity > 0)))
    with open(cache_file_path, 'w') as f:
        json.dump([c.to_dict() for c in cards], f)
    return cards


def get_timestamps():
    price_times = get_prices('8b18133c-3522-4922-9a49-4bed75c5a51a')
    timestamps = [p.timestamp for p in price_times]
    return timestamps


def parse_line(line):
    [card_id, updated_at, usd, eur, tix, usd_foil] = line.split(',')
    timestamp = parse(updated_at).timestamp() * 1000
    card_price = CardPrice(cardId=card_id, timestamp=timestamp, usd=float(usd), eur=float(eur),
                           tix=float(tix), usdFoil=float(usd_foil))
    return card_price


def get_values_at_timestamp(timestamp):
    cache_key = f'/tmp/{cache_break}values-at-{timestamp}'
    if os.path.exists(cache_key):
        with open(cache_key) as f:
            for line in f.readlines():
                yield parse_line(line)
    else:
        formatted_date = time.strftime(timestamp_format, time.gmtime(timestamp / 1000))
        key = f'{data_directory_root}/{formatted_date}.csv'
        obj = s3.Object(bucket, key)
        data = obj.get()['Body']
        with open(cache_key, 'w') as f:
            for line in [l.decode() for l in data.iter_lines()]:
                if 'update_time' not in line:
                    f.write(line)
                    print(parse_line(line))


def get_card_prices_at_timestamp(timestamp, card_ids):
    card_prices = {}
    for card_price in get_values_at_timestamp(timestamp):
        if card_price.cardId in card_ids:
            card_prices[card_price.cardId] = card_price
    return card_prices


def refresh_inventory_value(inventory_id, timestamp, is_last=False):
    cards = get_inventory_cards(inventory_id)
    inventory_value = InventoryValue(inventoryId=inventory_id, timestamp=timestamp, usd=0, eur=0, tix=0)
    card_prices = get_card_prices_at_timestamp(timestamp, [c.cardId for c in cards])
    for card in cards:
        card_price = card_prices.get(card.cardId)
        if card_price is not None:
            inventory_value.usd += card_price.usd * card.quantity
            inventory_value.usd += card_price.usdFoil * card.foilQuantity
            inventory_value.eur += card_price.eur * (card.quantity + card.foilQuantity)
            inventory_value.tix += card_price.tix * (card.quantity + card.foilQuantity)
            if is_last:
                changed = False
                if card_price.usd is not card.prices['usd']:
                    card.prices['usd'] = card_price.usd
                    changed = True
                if card_price.usdFoil is not card.prices['usdFoil']:
                    card.prices['usdFoil'] = card_price.usdFoil
                    changed = True
                if card_price.eur is not card.prices['eur']:
                    card.prices['eur'] = card_price.eur
                    changed = True
                if card_price.tix is not card.prices['tix']:
                    card.prices['tix'] = card_price.tix
                    changed = True
                if changed:
                    card.save()
                    cache_file_path = '/tmp/{0}inventory-{1}'.format(cache_break, inventory_id)
                    if os.path.exists(cache_file_path):
                        os.remove(cache_file_path)
    inventory_value.save()


def handle_get_timestamps(event, context):
    return {'statusCode': 200, 'body': json.dumps(get_timestamps())}


def handle_refresh_value(event, context):
    for record in event['Records']:
        message = record['body']
        refresh_request = json.loads(message)
        if 'inventoryId' in refresh_request:
            refresh_request['inventory_id'] = refresh_request.pop('inventoryId')
        refresh_inventory_value(**refresh_request)


patch_all()
