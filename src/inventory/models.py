from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute, ListAttribute, JSONAttribute, BooleanAttribute, \
    UnicodeSetAttribute


class BaseModel(Model):
    def to_dict(self):
        rval = {}
        for key in self.attribute_values:
            rval[key] = self.__getattribute__(key)
            if isinstance(rval[key], set):
                rval[key] = list(rval[key])
        return rval


class UserInventory(BaseModel):
    class Meta:
        """The meta details for the dynamodb model."""
        table_name = 'mtg-user-inventory'

    id = UnicodeAttribute(range_key=True, attr_name='inventoryId')
    userId = UnicodeAttribute(hash_key=True)
    name = UnicodeAttribute()
    createTime = NumberAttribute(default_for_new=0)
    sets = UnicodeSetAttribute(default_for_new=[])


class InventoryCard(BaseModel):
    def __init__(self, *initial_data, **kwargs):
        """The model class that contains the card details in an inventory for dynamodb."""
        super().__init__(**kwargs)
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    class Meta:

        """The meta details for the dynamodb model."""
        table_name = 'mtg-inventory-card'

    inventoryId = UnicodeAttribute(hash_key=True)
    cardId = UnicodeAttribute(range_key=True)
    quantity = NumberAttribute(default_for_new=0)
    foilQuantity = NumberAttribute(default_for_new=0)
    oracleId = UnicodeAttribute(null=True)
    name = UnicodeAttribute(null=True)
    scryfallUri = UnicodeAttribute(null=True)
    releasedAt = UnicodeAttribute(null=True)
    uri = UnicodeAttribute(null=True)
    layout = UnicodeAttribute(null=True)
    manaCost = UnicodeAttribute(null=True)
    cmc = NumberAttribute(null=True)
    typeLine = UnicodeAttribute(null=True)
    oracleText = UnicodeAttribute(null=True)
    power = UnicodeAttribute(null=True)
    toughness = UnicodeAttribute(null=True)
    colors = ListAttribute(null=True)
    colorIdentity = ListAttribute(null=True)
    imageUris = JSONAttribute(null=True)
    keywords = ListAttribute(null=True)
    foil = BooleanAttribute(null=True)
    nonfoil = BooleanAttribute(null=True)
    set = UnicodeAttribute(null=True)
    setName = UnicodeAttribute(null=True)
    setType = UnicodeAttribute(null=True)
    setUri = UnicodeAttribute(null=True)
    setSearchUri = UnicodeAttribute(null=True)
    scryfallSetUri = UnicodeAttribute(null=True)
    rulingsUri = UnicodeAttribute(null=True)
    printsSearchUri = UnicodeAttribute(null=True)
    collectorNumber = UnicodeAttribute(null=True)
    rarity = UnicodeAttribute(null=True)
    flavorText = UnicodeAttribute(null=True)
    cardBackId = UnicodeAttribute(null=True)
    artist = UnicodeAttribute(null=True)
    artistIds = ListAttribute(null=True)
    illustrationId = UnicodeAttribute(null=True)
    borderColor = UnicodeAttribute(null=True)
    frame = UnicodeAttribute(null=True)
    frameEffects = ListAttribute(null=True)
    fullArt = BooleanAttribute(null=True)
    textless = BooleanAttribute(null=True)
    booster = BooleanAttribute(null=True)
    edhrecRank = NumberAttribute(null=True)
    allParts = JSONAttribute(null=True)
    cardFaces = JSONAttribute(null=True)
    prices = JSONAttribute(null=True)
    createdDate = NumberAttribute()
    updatedDate = NumberAttribute()


class UserAudit(BaseModel):
    class Meta:
        """The meta details for the dynamodb model."""
        table_name = 'mtg-user-audit'

    userId = UnicodeAttribute(hash_key=True)
    auditTimestampMs = NumberAttribute(range_key=True)
    inventory = JSONAttribute(null=True)
    type = UnicodeAttribute()
    modifyCardRequest = JSONAttribute(null=True)
    moveCardRequest = JSONAttribute(null=True)


class CardPrice(BaseModel):
    def __init__(self, *initial_data, **kwargs):
        """The model class that contains the card details in an inventory for dynamodb."""
        super().__init__(**kwargs)
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    class Meta:

        """The meta details for the dynamodb model."""
        table_name = 'mtg-card-price'

    cardId = UnicodeAttribute(hash_key=True)
    timestamp = NumberAttribute(range_key=True)
    usd = NumberAttribute(default_for_new=0)
    eur = NumberAttribute(default_for_new=0)
    tix = NumberAttribute(default_for_new=0)
    usdFoil = NumberAttribute(default_for_new=0)


class InventoryValue(BaseModel):
    def __init__(self, *initial_data, **kwargs):
        """The model class that contains the card details in an inventory for dynamodb."""
        super().__init__(**kwargs)
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    class Meta:

        """The meta details for the dynamodb model."""
        table_name = 'mtg-inventory-value'

    inventoryId = UnicodeAttribute(hash_key=True)
    timestamp = NumberAttribute(range_key=True)
    usd = NumberAttribute(default_for_new=0)
    eur = NumberAttribute(default_for_new=0)
    tix = NumberAttribute(default_for_new=0)
