import json
import os
from aws_xray_sdk.core import patch_all
import boto3
from models import UserInventory, InventoryCard, UserAudit, InventoryValue
import uuid
import time
import logging

logging.getLogger().setLevel("DEBUG")
pynamodb_logger = logging.getLogger("pynamodb")
pynamodb_logger.setLevel(logging.DEBUG)
pynamodb_logger.propagate = True
authentication_arn = os.getenv("AUTHENTICATION_FUNCTION")
get_card_arn = os.getenv("GET_CARD_FUNCTION")
lambda_client = boto3.client('lambda')
sqs = boto3.resource('sqs')

update_inventory_queue = os.getenv("UPDATE_INVENTORY_QUEUE")
if update_inventory_queue is not None:
    update_queue = sqs.get_queue_by_name(QueueName=update_inventory_queue)


class AuthenticationError(Exception):
    def __init__(self, authentication_response):
        """This class is used to map an authentication error into an exception."""
        for key in authentication_response:
            setattr(self, key, authentication_response[key])


def get_authentication(token):
    payload = json.dumps({'token': token})
    response = lambda_client.invoke(
        FunctionName=authentication_arn,
        Payload=bytes(payload, 'UTF-8')
    )
    response = json.loads(response['Payload'].read().decode())
    if response['statusCode'] != 200:
        raise AuthenticationError(response)
    return response


def get_token(event):
    return event['headers'].get('Authorization')


def handle_get_inventories(event, context):
    token = get_token(event)
    try:
        user_id = json.loads(get_authentication(token)['body'])['sub']
        inventories = UserInventory.query(user_id)
        inventories = [i.to_dict() for i in inventories]
        return {'statusCode': 200, 'body': json.dumps(inventories)}
    except AuthenticationError as e:
        return e.__dict__


def handle_get_inventory(event, context):
    inventory_id = event['pathParameters']['inventoryId']
    query_parameters = event['queryStringParameters']
    logging.debug('loading {}'.format(inventory_id))
    if query_parameters is not None and query_parameters['set'] is not None:
        set_id = query_parameters['set']
        logging.debug('with set {}'.format(set_id))
        cards = InventoryCard.query(inventory_id, filter_condition=InventoryCard.set == set_id)
        cards = filter((lambda x: x.quantity + x.foilQuantity != 0), cards)
    else:
        cards = InventoryCard.query(inventory_id,
                                    filter_condition=(InventoryCard.quantity > 0) | (InventoryCard.foilQuantity > 0))
    cards = [c.to_dict() for c in cards]
    return {'statusCode': 200, 'body': json.dumps(cards)}


def handle_get_inventory_value(event, context):
    inventory_id = event['pathParameters']['inventoryId']
    values = InventoryValue.query(inventory_id)
    values = [v.to_dict() for v in values]
    return {'statusCode': 200, 'body': json.dumps(values)}


def handle_create_inventory(event, context):
    if 'body' not in event:
        return {'statusCode': 400, 'body': 'A name is required to create an inventory'}
    body = json.loads(event['body'])
    if 'name' not in body:
        return {'statusCode': 400, 'body': 'A name is required to create an inventory'}
    token = get_token(event)
    try:
        user_id = json.loads(get_authentication(token)['body'])['sub']
    except AuthenticationError as e:
        return e.__dict__
    inventory = UserInventory(user_id, str(uuid.uuid4()),
                              name=body['name'],
                              sets={},
                              createTime=int(round(time.time() * 1000)))
    inventory.save()
    return {'statusCode': 200, 'body': json.dumps(inventory.to_dict())}


def process_modify_card_request(event):
    if 'body' not in event:
        return {'statusCode': 400, 'body': 'Missing request body'}, None, None, None, None
    request = json.loads(event['body'])
    if 'card' not in request:
        return {'statusCode': 400, 'body': 'Card missing in request'}, None, None, None, None
    if 'quantity' not in request or request['quantity'] < 0:
        return {'statusCode': 400, 'body': 'Card quantity must be more that 0'}, None, None, None, None
    token = get_token(event)
    try:
        user_id = json.loads(get_authentication(token)['body'])['sub']
    except AuthenticationError as e:
        return e.__dict__, None, None, None, None
    inventory = UserInventory.get(user_id, event['pathParameters']['inventoryId'])
    if not inventory:
        return {'statusCode': 401, 'body': 'you are not authorized to modify this inventory'}, None, None, None, None
    inventory_card = get_inventory_card(request['card'], inventory.id)
    return None, request, inventory_card, inventory, user_id


def get_inventory_card(card, inventory_id):
    loaded_card = False
    if 'set' in card and 'number' in card:
        logging.debug('Number and set provided in card so loading card from scryfall to get id')
        loaded_card = lambda_client.invoke(
            FunctionName=get_card_arn,
            Payload=bytes(json.dumps(card), 'UTF-8')
        )
        loaded_card = json.loads(loaded_card['Payload'].read().decode())
        card['id'] = loaded_card['id']

    try:
        inventory_card = InventoryCard.get(inventory_id, card['id'])
    except InventoryCard.DoesNotExist:
        logging.info("Card {} did not exist in inventory".format(card['id']))
        if not loaded_card:
            loaded_card = lambda_client.invoke(
                FunctionName=get_card_arn,
                Payload=bytes(json.dumps(card), 'UTF-8')
            )
            loaded_card = json.loads(loaded_card['Payload'].read().decode())
        loaded_card['cardId'] = loaded_card.pop('id')
        inventory_card = InventoryCard(loaded_card, inventoryId=inventory_id, createdDate=time.time() * 1000)
    return inventory_card


def handle_add_card(event, context):
    error, request, inventory_card, inventory, user_id = process_modify_card_request(event)
    if error:
        return error
    quantity = request['quantity']
    if request['foil']:
        delta_quantity, delta_foil_quantity = 0, quantity
        inventory_card.foilQuantity += quantity
    else:
        delta_quantity, delta_foil_quantity = quantity, 0
        inventory_card.quantity += quantity
    update_time = time.time() * 1000
    inventory_card.updatedDate = update_time
    inventory_card.save()
    inventory.save()
    UserAudit(user_id, update_time, userId=user_id, inventory=inventory.to_dict(), type='Add Card',
              modifyCardRequest=request).save()
    update_inventory(inventory.id, "{}*{}".format(user_id, update_time), card_id=inventory_card.cardId,
                     quantity=delta_quantity, foil_quantity=delta_foil_quantity)
    return {'statusCode': 200, 'body': json.dumps(inventory_card.to_dict())}


def handle_remove_card(event, context):
    error, request, inventory_card, inventory, user_id = process_modify_card_request(event)
    if error:
        return error
    quantity = request['quantity']
    if request['foil']:
        if quantity > inventory_card.foilQuantity:
            return {'statusCode': 400, 'body': 'You can not remove more cards than you currently have'}
        delta_quantity, delta_foil_quantity = 0, quantity * -1
        inventory_card.foilQuantity -= quantity
    else:
        if quantity > inventory_card.quantity:
            return {'statusCode': 400, 'body': 'You can not remove more cards than you currently have'}
        delta_quantity, delta_foil_quantity = quantity * -1, 0
        inventory_card.quantity -= quantity
    update_time = time.time() * 1000
    inventory_card.updatedDate = update_time
    inventory_card.save()
    UserAudit(user_id, update_time, userId=user_id, inventory=inventory.to_dict(), type='Remove Card',
              modifyCardRequest=request).save()
    update_inventory(inventory.id, "{}*{}".format(user_id, update_time), card_id=inventory_card.cardId,
                     quantity=delta_quantity, foil_quantity=delta_foil_quantity)
    return {'statusCode': 200, 'body': json.dumps(inventory_card.to_dict())}


def handle_set_card_quantity(event, context):
    error, request, inventory_card, inventory, user_id = process_modify_card_request(event)
    if error:
        return error
    quantity = request['quantity']
    if request['foil']:
        delta_quantity, delta_foil_quantity = 0, quantity - inventory_card.foilQuantity
        inventory_card.foilQuantity = quantity
    else:
        delta_quantity, delta_foil_quantity = quantity - inventory_card.quantity, 0
        inventory_card.quantity = quantity
    update_time = time.time() * 1000
    inventory_card.updatedDate = update_time
    inventory_card.save()
    UserAudit(user_id, update_time, userId=user_id, inventory=inventory.to_dict(), type='Set Card Quantity',
              modifyCardRequest=request).save()
    update_inventory(inventory.id, "{}*{}".format(user_id, update_time), card_id=inventory_card.cardId,
                     quantity=delta_quantity, foil_quantity=delta_foil_quantity)
    return {'statusCode': 200, 'body': json.dumps(inventory_card.to_dict())}


def handle_move_cards(event, context):
    if 'body' not in event:
        return {'statusCode': 400, 'body': 'Missing request body'}
    request = json.loads(event['body'])
    if 'card' not in request:
        return {'statusCode': 400, 'body': 'Card missing in request'}
    token = get_token(event)
    try:
        user_id = json.loads(get_authentication(token)['body'])['sub']
    except AuthenticationError as e:
        return e.__dict__
    from_inventory = UserInventory.get(user_id, request['fromInventory'])
    if not from_inventory:
        return {'statusCode': 401, 'body': 'you are not authorized to remove from this inventory'}
    to_inventory = UserInventory.get(user_id, request['toInventory'])
    if not to_inventory:
        return {'statusCode': 401, 'body': 'you are not authorized to add to this inventory'}
    from_inventory_card = get_inventory_card(request['card'], from_inventory.id)
    to_inventory_card = get_inventory_card(request['card'], to_inventory.id)
    if request['foil']:
        moved_quantity, moved_foil_quantity = 0, from_inventory_card.foilQuantity
        to_inventory_card.foilQuantity = from_inventory_card.foilQuantity
        from_inventory_card.foilQuantity = 0
    else:
        moved_quantity, moved_foil_quantity = from_inventory_card.quantity, 0
        to_inventory_card.quantity = from_inventory_card.quantity
        from_inventory_card.quantity = 0
    update_time = time.time() * 1000
    from_inventory_card.updatedDate = update_time
    to_inventory_card.updatedDate = update_time
    from_inventory_card.save()
    to_inventory_card.save()
    UserAudit(user_id, update_time, userId=user_id, inventory=from_inventory.to_dict(), type='Move Card',
              moveCardRequest=request).save()
    card_id = to_inventory_card.cardId
    update_inventory(from_inventory.id, "{}*{}".format(user_id, update_time), card_id=card_id, quantity=moved_quantity,
                     foil_quantity=moved_foil_quantity)
    update_inventory(to_inventory.id, "{}*{}".format(user_id, update_time), card_id=card_id * -1,
                     quantity=moved_quantity * -1, foil_quantity=moved_foil_quantity)
    return {'statusCode': 200, 'body': json.dumps(from_inventory_card.to_dict())}


def update_inventory(inventory_id, audit_key, card_id, quantity=0, foil_quantity=0):
    request = {'inventory_id': inventory_id, 'card_id': card_id, 'quantity': quantity, 'foil_quantity': foil_quantity}
    message_body = json.dumps(request)
    update_queue.send_message(MessageBody=message_body, MessageGroupId=inventory_id,
                              MessageDeduplicationId='{}|{}'.format(audit_key, card_id))


patch_all()
