from aws_xray_sdk.core import patch_all
import json
from models import InventoryValue


def handle_lambda(event, context):
    for record in event['Records']:
        message = record['body']
        InventoryValue(json.loads(message)).save()


patch_all()
