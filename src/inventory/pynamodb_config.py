connect_timeout_seconds = 15
read_timeout_seconds = 30
max_retry_attempts = 5
base_backoff_ms = 100
max_pool_connections = 10
extra_headers = None