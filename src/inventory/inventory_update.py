from aws_xray_sdk.core import patch_all
import json
import boto3
import os

from models import CardPrice, InventoryValue

write_queue_name = os.getenv("WRITE_VALUE_QUEUE")
sqs = boto3.resource('sqs')
if write_queue_name is not None:
    write_queue = sqs.get_queue_by_name(QueueName=write_queue_name)
refresh_queue_name = os.getenv("REFRESH_INVENTORY_VALUE_QUEUE")
if refresh_queue_name is not None:
    refresh_queue = sqs.get_queue_by_name(QueueName=refresh_queue_name)


def get_prices(card_id):
    return CardPrice.query(card_id)


def update_inventory(update_request):
    inventory_id = update_request['inventory_id']
    card_id = update_request['card_id']
    quantity = update_request.get('quantity', 0)
    foil_quantity = update_request.get('foil_quantity', 0)
    inventory_values_by_time = {v.timestamp: v for v in InventoryValue.query(inventory_id)}
    entries = []
    for price_at_time in get_prices(card_id):
        if price_at_time.timestamp in inventory_values_by_time:
            inventory_value = inventory_values_by_time[price_at_time.timestamp]
            inventory_value.usd += price_at_time.usd * quantity
            inventory_value.usd += price_at_time.usdFoil * foil_quantity
            inventory_value.eur += price_at_time.eur * (foil_quantity + quantity)
            inventory_value.tix += price_at_time.tix * (foil_quantity + quantity)
            entries.append({'Id': str(price_at_time.timestamp), 'MessageBody': json.dumps(inventory_value.to_dict())})
            if len(entries) == 10:
                write_queue.send_messages(Entries=entries)
                entries = []
        else:
            refresh_queue.send_message(
                MessageBody=json.dumps({'inventoryId': inventory_id, 'timestamp': price_at_time.timestamp}))

    if len(entries) > 0:
        write_queue.send_messages(Entries=entries)


def handle_lambda(event, context):
    for record in event['Records']:
        message = record['body']
        update_request = json.loads(message)
        update_inventory(update_request)


patch_all()
