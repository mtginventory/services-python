import json
import http.client
from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

client = http.client.HTTPSConnection("api.scryfall.com")


@xray_recorder.capture('get-types')
def get_types():
    return ["card-names", "artist-names", "word-bank", "creature-types",
            "planeswalker-types", "land-types", "artifact-types", "enchantment-types", "spell-types", "powers",
            "toughnesses", "loyalties", "watermarks", "keyword-abilities", "keyword-actions", "ability-words"]


@xray_recorder.capture('get-type')
def get_data(data_type):
    client.request('GET', '/catalog/{}'.format(data_type))
    response = client.getresponse()
    body = json.loads(response.read())
    return body['data']


def handle_get_types(event, context):
    return {'statusCode': 200, 'body': json.dumps(get_types())}


def handle_get_catalog(event, context):
    path_parameters = event['pathParameters']
    if path_parameters is None or 'catalogType' not in path_parameters:
        return {'statusCode': 400, 'body': 'You need to provide a catalog type'}
    output = get_data(path_parameters['catalogType'])
    return {'statusCode': 200, 'body': json.dumps(output)}
